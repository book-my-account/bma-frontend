ARG NODE=12.18.2-stretch-slim

FROM node:${NODE}

RUN apt update \
  && apt -y install sudo nano git

ENV ANGULARCLI=9.1.10

RUN npm install -g @angular/cli@${ANGULARCLI}

EXPOSE 4200

WORKDIR /usr/src/bma-frontend

ENTRYPOINT ["/bin/bash"]
